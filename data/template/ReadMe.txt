Thank you for downloading Light Ship!

How to Play
**Use the arrowkeys to move
**Use spacekey to trigger things
**Try to colour as many tiles
  as possible!

Planned updates for 1.9
**AI Players

Planned updates for the future
**Split screen
**mines power up
**bump power up
**Documentation for scripting
**Map description on selection

Credits
**Code - TheComet
**Graphics - TheComet
**Music - Renard (Kitsune^2)

Shoutouts
**Adam - Testing and feedback
**David - Testing and feedback
**Melancholic - Testing and feedback
**TheGameCreators.com - Awesomeness!
**Freeze - Awesome map!

Software
**DarkBASIC Pro
**Advanced2D
**Matrix1 Utils
**multijoy
**IAA Astar pathfinding DLL
**E V O L V E D shaders
**Sparky's Collision DLL
**The GIMP
**AC3D
**CharacterFX