//====================================================
// Parallax mapping
//====================================================
// By EVOLVED
// www.evolved-software.com
//====================================================

//--------------
// un-tweaks
//--------------
   matrix WorldVP:WorldViewProjection;
   matrix World:World;   
   matrix ViewInv:ViewInverse; 

//--------------
// tweaks
//--------------
   float3 Ambient={0.2f,0.2f,0.2f};
   float3 LightPosition={150.0f,150.0f,0.0f};
   float3 LightColor={1.0f,1.0f,1.0f};
   float LightRange=50.0f;
   float SpecularPow=32.0f;
   float Heightvec=0.03;
   float BiasHeight=0.5;
   float4 FogColor={0.8f,0.8f,0.8f,1.0f};
   float FogRange=1200.0f;
   float Alpha=1.0f;

   float EmissPower = 1.0f;

//--------------
// Textures
//--------------
   texture BaseTX <string Name="";>;	
   sampler2D Base = sampler_state 
    {
 	texture = <BaseTX>;
    };
   texture NormalTX <string Name="";>;	
   sampler2D Normal = sampler_state 
    {
 	texture = <NormalTX>;
    };
   texture HeightTX <string Name="";>;
   sampler2D Height = sampler_state 
    {
 	texture = <HeightTX>;
    };
   texture EmissiveTX <string Name="";>;
   sampler2D EmissiveMap = sampler_state 
    {
 	texture = <EmissiveTX>;
    };
//--------------
// structs 
//--------------
   struct input
     {
 	float4 Pos:POSITION; 
 	float2 UV:TEXCOORD; 
	float3 Normal:NORMAL;
 	float3 Tangent:TANGENT;
 	float3 Binormal:BINORMAL;
     };
   struct output
     {
	float4 OPos:POSITION; 
 	float2 Tex:TEXCOORD0; 
  	float3 LightVec:TEXCOORD1; 
	float3 Attenuation:TEXCOORD2;
	float3 ViewVec:TEXCOORD3; 
	float Fog:FOG;
     };

//--------------
// vertex shader
//--------------
   output VS(input IN) 
     {
 	output OUT;
	OUT.OPos=mul(IN.Pos,WorldVP); 
 	OUT.Tex=IN.UV;
	float3x3 TBN={IN.Tangent,IN.Binormal,IN.Normal};
	TBN=transpose(mul(TBN,World));
	float3 WPos=mul(IN.Pos,World); 
	float3 LightPos=LightPosition-WPos; 
	float3 ViewPos=ViewInv[3].xyz-WPos;
	OUT.LightVec=mul(LightPos,TBN);
	OUT.Attenuation=-LightPos/LightRange; 
 	OUT.ViewVec=mul(ViewPos,TBN);  
	OUT.Fog=1-saturate(dot(ViewPos/FogRange,ViewPos/FogRange));
	return OUT;
     }

//--------------
// pixel shader
//--------------
    float4 PS(output IN)  : COLOR
     {
	float3 View=normalize(IN.ViewVec);
	float HeightTex=tex2D(Height,IN.Tex).x+BiasHeight; 
	float2 NewUv=(Heightvec*HeightTex-0.02)*View+IN.Tex; 
	float4 Texture=tex2D(Base,NewUv);
	float3 Emissive=tex2D(EmissiveMap,NewUv).xyz; 
	float3 NormalMap=tex2D(Normal,NewUv)*2-1; 
	NormalMap=normalize(NormalMap);
	float3 LightV=normalize(IN.LightVec);
	float Normal=saturate(dot(reflect(-View,NormalMap),LightV));
	Normal=pow(Normal,SpecularPow)+saturate(dot(NormalMap,LightV)); 
	float PixelLight=1-saturate(dot(IN.Attenuation,IN.Attenuation));
	float3 Light=PixelLight*LightColor;
	return float4( (Texture*((Normal*Light)+Ambient))+(Emissive*EmissPower),Texture.w*Alpha);
     }

//--------------
// techniques   
//--------------
   technique Parallax 
      {
 	pass p1
      {		
 	vertexShader = compile vs_2_0 VS(); 
 	pixelShader  = compile ps_2_0 PS();
	FOGCOLOR=(FogColor); 
	FOGENABLE=TRUE;		
      }
      }