//====================================================
// Parallax mapping w/ Emissive
//====================================================
// By EVOLVED - www.evolved-software.com
// Modified By: Revenant_Chaos
//====================================================

//--------------
// un-tweaks
//--------------
   matrix WorldVP:WorldViewProjection; 
   matrix World:World;   
   matrix ViewInv:ViewInverse; 

//--------------
// tweaks
//--------------
   float3 Ambient={0.2f,0.2f,0.2f};
   float3 LightPosition_1={150.0f,150.0f,0.0f};    
   float3 LightColor_1={1.0f,1.0f,1.0f};    
   float LightRange_1=50.0f;
   float3 LightPosition_2={150.0f,150.0f,0.0f};    
   float3 LightColor_2={1.0f,0.0f,0.0f};    
   float LightRange_2=50.0f;    
   float3 LightPosition_3={150.0f,150.0f,0.0f};    
   float3 LightColor_3={0.0f,1.0f,0.0f};    
   float LightRange_3=50.0f;    
   float3 LightPosition_4={150.0f,150.0f,0.0f};    
   float3 LightColor_4={0.0f,0.0f,1.0f};    
   float LightRange_4=50.0f; 
   float3 LightPosition_5={150.0f,150.0f,0.0f};    
   float3 LightColor_5={1.0f,0.0f,1.0f};    
   float LightRange_5=50.0f;    
   float3 LightPosition_6={150.0f,150.0f,0.0f};    
   float3 LightColor_6={0.0f,1.0f,1.0f};    
   float LightRange_6=50.0f; 
   float SpecularPow=32.0f;  
   float Heightvec=0.03;
   float BiasHeight=0.6;
   float Alpha=1.0f;
   float EmissPower = 1.0f;

//--------------
// Textures
//--------------
   texture BaseTX <string Name="";>;	
   sampler2D Base = sampler_state 
    {
 	texture = <BaseTX>;
    };
   texture NormalTX <string Name="";>;	
   sampler2D Normal = sampler_state 
    {
 	texture = <NormalTX>;
    };
   texture HeightTX <string Name="";>;	
   sampler2D Height = sampler_state 
    {
 	texture = <HeightTX>;
    };
   texture EmissiveTX <string Name="";>;	
   sampler2D Emiss = sampler_state 
    {
 	texture = <EmissiveTX>;
    };

//--------------
// structs 
//--------------
   struct input
     {
 	float4 Pos:POSITION; 
 	float2 UV:TEXCOORD; 
	float3 Normal:NORMAL;
 	float3 Tangent:TANGENT;
 	float3 Binormal:BINORMAL;
     };
   struct output
     {
	float4 OPos:POSITION; 
 	float2 Tex:TEXCOORD0; 
  	float3 LightVec:TEXCOORD1; 
	float3 Attenuation:TEXCOORD2;
	float3 ViewVec:TEXCOORD3; 
     };

//--------------
// vertex shader
//--------------
   output VS(input IN) 
     {
 	output OUT;
	OUT.OPos=mul(IN.Pos,WorldVP); 
 	OUT.Tex=IN.UV;
	OUT.LightVec=0.0f;
	OUT.Attenuation=0.0f; 
	float3x3 TBN={IN.Tangent,IN.Binormal,IN.Normal};
	TBN=transpose(mul(TBN,World));
	float3 WPos=mul(IN.Pos,World);
	float3 ViewPos=ViewInv[3].xyz-WPos;
 	OUT.ViewVec=mul(ViewPos,TBN);
	return OUT;
     }
   output VS_L1(input IN) 
     {
 	output OUT;
	OUT.OPos=mul(IN.Pos,WorldVP); 
 	OUT.Tex=IN.UV;
	float3x3 TBN={IN.Tangent,IN.Binormal,IN.Normal};
	TBN=transpose(mul(TBN,World));
	float3 WPos=mul(IN.Pos,World); 
	float3 LightPos=LightPosition_1-WPos; 
	float3 ViewPos=ViewInv[3].xyz-WPos;
	OUT.LightVec=mul(LightPos,TBN);
	OUT.Attenuation=-LightPos/LightRange_1; 
 	OUT.ViewVec=mul(ViewPos,TBN);  
	return OUT;
     }
   output VS_L2(input IN) 
     {
 	output OUT;
	OUT.OPos=mul(IN.Pos,WorldVP); 
 	OUT.Tex=IN.UV;
	float3x3 TBN={IN.Tangent,IN.Binormal,IN.Normal};
	TBN=transpose(mul(TBN,World));
	float3 WPos=mul(IN.Pos,World); 
	float3 LightPos=LightPosition_2-WPos; 
	float3 ViewPos=ViewInv[3].xyz-WPos;
	OUT.LightVec=mul(LightPos,TBN);
	OUT.Attenuation=-LightPos/LightRange_2; 
 	OUT.ViewVec=mul(ViewPos,TBN);  
	return OUT;
     }
   output VS_L3(input IN) 
     {
 	output OUT;
	OUT.OPos=mul(IN.Pos,WorldVP); 
 	OUT.Tex=IN.UV;
	float3x3 TBN={IN.Tangent,IN.Binormal,IN.Normal};
	TBN=transpose(mul(TBN,World));
	float3 WPos=mul(IN.Pos,World); 
	float3 LightPos=LightPosition_3-WPos; 
	float3 ViewPos=ViewInv[3].xyz-WPos;
	OUT.LightVec=mul(LightPos,TBN);
	OUT.Attenuation=-LightPos/LightRange_3; 
 	OUT.ViewVec=mul(ViewPos,TBN);  
	return OUT;
     }
   output VS_L4(input IN) 
     {
 	output OUT;
	OUT.OPos=mul(IN.Pos,WorldVP); 
 	OUT.Tex=IN.UV;
	float3x3 TBN={IN.Tangent,IN.Binormal,IN.Normal};
	TBN=transpose(mul(TBN,World));
	float3 WPos=mul(IN.Pos,World); 
	float3 LightPos=LightPosition_4-WPos; 
	float3 ViewPos=ViewInv[3].xyz-WPos;
	OUT.LightVec=mul(LightPos,TBN);
	OUT.Attenuation=-LightPos/LightRange_4; 
 	OUT.ViewVec=mul(ViewPos,TBN);  
	return OUT;
     }
   output VS_L5(input IN) 
     {
 	output OUT;
	OUT.OPos=mul(IN.Pos,WorldVP); 
 	OUT.Tex=IN.UV;
	float3x3 TBN={IN.Tangent,IN.Binormal,IN.Normal};
	TBN=transpose(mul(TBN,World));
	float3 WPos=mul(IN.Pos,World); 
	float3 LightPos=LightPosition_5-WPos; 
	float3 ViewPos=ViewInv[3].xyz-WPos;
	OUT.LightVec=mul(LightPos,TBN);
	OUT.Attenuation=-LightPos/LightRange_5; 
 	OUT.ViewVec=mul(ViewPos,TBN);  
	return OUT;
     }
   output VS_L6(input IN) 
     {
 	output OUT;
	OUT.OPos=mul(IN.Pos,WorldVP); 
 	OUT.Tex=IN.UV;
	float3x3 TBN={IN.Tangent,IN.Binormal,IN.Normal};
	TBN=transpose(mul(TBN,World));
	float3 WPos=mul(IN.Pos,World); 
	float3 LightPos=LightPosition_6-WPos; 
	float3 ViewPos=ViewInv[3].xyz-WPos;
	OUT.LightVec=mul(LightPos,TBN);
	OUT.Attenuation=-LightPos/LightRange_6; 
 	OUT.ViewVec=mul(ViewPos,TBN);  
	return OUT;
     }

//--------------
// pixel shader
//--------------
    float4 PS(output IN)  : COLOR
     {
	float3 View=normalize(IN.ViewVec);
	float HeightTex=tex2D(Height,IN.Tex).x+BiasHeight; 
	float2 NewUv=(Heightvec*HeightTex-0.02)*View+IN.Tex; 
	float4 Texture=tex2D(Base,NewUv);
	float3 Emissive=tex2D(Emiss,NewUv).xyz;
	return float4((Texture.xyz*Ambient)+(Emissive*EmissPower),Texture.w*Alpha);
     }
    float4 PS_L1(output IN)  : COLOR
     {
	float3 View=normalize(IN.ViewVec);
	float HeightTex=tex2D(Height,IN.Tex).x+BiasHeight; 
	float2 NewUv=(Heightvec*HeightTex-0.02)*View+IN.Tex; 
	float4 Texture=tex2D(Base,NewUv);
	float3 Emissive=tex2D(Emiss,NewUv).xyz; 
	float3 NormalMap=tex2D(Normal,NewUv)*2-1; 
	NormalMap=normalize(NormalMap);
	float3 LightV=normalize(IN.LightVec);
	float Normal=saturate(dot(reflect(-View,NormalMap),LightV));
	Normal=pow(Normal,SpecularPow)+saturate(dot(NormalMap,LightV)); 
	float PixelLight=1-saturate(dot(IN.Attenuation,IN.Attenuation));
	float3 Light=PixelLight*LightColor_1;
	return float4( (Texture.xyz*((Normal*Light)+Ambient))+(Emissive*EmissPower),Texture.w*Alpha);
     }
    float4 PS_L2(output IN)  : COLOR
     {
	float3 View=normalize(IN.ViewVec);
	float HeightTex=tex2D(Height,IN.Tex).x+BiasHeight; 
	float2 NewUv=(Heightvec*HeightTex-0.02)*View+IN.Tex; 
	float4 Texture=tex2D(Base,NewUv);
	float3 NormalMap=tex2D(Normal,NewUv)*2-1; 
	NormalMap=normalize(NormalMap);
	float3 LightV=normalize(IN.LightVec);
	float Normal=saturate(dot(reflect(-View,NormalMap),LightV));
	Normal=pow(Normal,SpecularPow)+saturate(dot(NormalMap,LightV)); 
	float PixelLight=1-saturate(dot(IN.Attenuation,IN.Attenuation));
	float3 Light=PixelLight*LightColor_2;
	return float4(Texture.xyz*(Normal*Light),Texture.w*Alpha);
     }
    float4 PS_L3(output IN)  : COLOR
     {
	float3 View=normalize(IN.ViewVec);
	float HeightTex=tex2D(Height,IN.Tex).x+BiasHeight; 
	float2 NewUv=(Heightvec*HeightTex-0.02)*View+IN.Tex; 
	float4 Texture=tex2D(Base,NewUv);
	float3 NormalMap=tex2D(Normal,NewUv)*2-1; 
	NormalMap=normalize(NormalMap);
	float3 LightV=normalize(IN.LightVec);
	float Normal=saturate(dot(reflect(-View,NormalMap),LightV));
	Normal=pow(Normal,SpecularPow)+saturate(dot(NormalMap,LightV)); 
	float PixelLight=1-saturate(dot(IN.Attenuation,IN.Attenuation));
	float3 Light=PixelLight*LightColor_3;
	return float4(Texture.xyz*(Normal*Light),Texture.w*Alpha);
     }
    float4 PS_L4(output IN)  : COLOR
     {
	float3 View=normalize(IN.ViewVec);
	float HeightTex=tex2D(Height,IN.Tex).x+BiasHeight; 
	float2 NewUv=(Heightvec*HeightTex-0.02)*View+IN.Tex; 
	float4 Texture=tex2D(Base,NewUv);
	float3 NormalMap=tex2D(Normal,NewUv)*2-1; 
	NormalMap=normalize(NormalMap);
	float3 LightV=normalize(IN.LightVec);
	float Normal=saturate(dot(reflect(-View,NormalMap),LightV));
	Normal=pow(Normal,SpecularPow)+saturate(dot(NormalMap,LightV)); 
	float PixelLight=1-saturate(dot(IN.Attenuation,IN.Attenuation));
	float3 Light=PixelLight*LightColor_4;
	return float4(Texture.xyz*(Normal*Light),Texture.w*Alpha);
     }
    float4 PS_L5(output IN)  : COLOR
     {
	float3 View=normalize(IN.ViewVec);
	float HeightTex=tex2D(Height,IN.Tex).x+BiasHeight; 
	float2 NewUv=(Heightvec*HeightTex-0.02)*View+IN.Tex; 
	float4 Texture=tex2D(Base,NewUv);
	float3 NormalMap=tex2D(Normal,NewUv)*2-1; 
	NormalMap=normalize(NormalMap);
	float3 LightV=normalize(IN.LightVec);
	float Normal=saturate(dot(reflect(-View,NormalMap),LightV));
	Normal=pow(Normal,SpecularPow)+saturate(dot(NormalMap,LightV)); 
	float PixelLight=1-saturate(dot(IN.Attenuation,IN.Attenuation));
	float3 Light=PixelLight*LightColor_5;
	return float4(Texture.xyz*(Normal*Light),Texture.w*Alpha);
     }
    float4 PS_L6(output IN)  : COLOR
     {
	float3 View=normalize(IN.ViewVec);
	float HeightTex=tex2D(Height,IN.Tex).x+BiasHeight; 
	float2 NewUv=(Heightvec*HeightTex-0.02)*View+IN.Tex; 
	float4 Texture=tex2D(Base,NewUv);
	float3 NormalMap=tex2D(Normal,NewUv)*2-1; 
	NormalMap=normalize(NormalMap);
	float3 LightV=normalize(IN.LightVec);
	float Normal=saturate(dot(reflect(-View,NormalMap),LightV));
	Normal=pow(Normal,SpecularPow)+saturate(dot(NormalMap,LightV)); 
	float PixelLight=1-saturate(dot(IN.Attenuation,IN.Attenuation));
	float3 Light=PixelLight*LightColor_6;
	return float4(Texture.xyz*(Normal*Light),Texture.w*Alpha);
     }

//--------------
// techniques   
//--------------
   technique Ambient
      {
 	pass p1
      {		
 	vertexShader = compile vs_2_0 VS(); 
 	pixelShader  = compile ps_2_0 PS();
      }
      }
   technique Light1
      {
 	pass p1
      {		
 	vertexShader = compile vs_2_0 VS_L1(); 
 	pixelShader  = compile ps_2_0 PS_L1();
      }
      }
   technique Light2
      {
 	pass p1
      {		
 	vertexShader = compile vs_2_0 VS_L1(); 
 	pixelShader  = compile ps_2_0 PS_L1();
      }
 	pass p2
      {		
 	vertexShader = compile vs_2_0 VS_L2(); 
 	pixelShader  = compile ps_2_0 PS_L2();
	AlphaBlendEnable = True;
 	SrcBlend = One;
 	DestBlend = One;
      }
      }
   technique Light3
      {
 	pass p1
      {		
 	vertexShader = compile vs_2_0 VS_L1(); 
 	pixelShader  = compile ps_2_0 PS_L1();
      }
 	pass p2
      {		
 	vertexShader = compile vs_2_0 VS_L2(); 
 	pixelShader  = compile ps_2_0 PS_L2();
	AlphaBlendEnable = True;
 	SrcBlend = One;
 	DestBlend = One;
      }
 	pass p3
      {		
 	vertexShader = compile vs_2_0 VS_L3(); 
 	pixelShader  = compile ps_2_0 PS_L3();
	AlphaBlendEnable = True;
 	SrcBlend = One;
 	DestBlend = One;
      }
      }
   technique Light4
      {
 	pass p1
      {		
 	vertexShader = compile vs_2_0 VS_L1(); 
 	pixelShader  = compile ps_2_0 PS_L1();
      }
 	pass p2
      {		
 	vertexShader = compile vs_2_0 VS_L2(); 
 	pixelShader  = compile ps_2_0 PS_L2();
	AlphaBlendEnable = True;
 	SrcBlend = One;
 	DestBlend = One;
      }
 	pass p3
      {		
 	vertexShader = compile vs_2_0 VS_L3(); 
 	pixelShader  = compile ps_2_0 PS_L3();
	AlphaBlendEnable = True;
 	SrcBlend = One;
 	DestBlend = One;
      }
 	pass p4
      {		
 	vertexShader = compile vs_2_0 VS_L4(); 
 	pixelShader  = compile ps_2_0 PS_L4();
	AlphaBlendEnable = True;
 	SrcBlend = One;
 	DestBlend = One;
      }
      }
   technique Light5
      {
 	pass p1
      {		
 	vertexShader = compile vs_2_0 VS_L1(); 
 	pixelShader  = compile ps_2_0 PS_L1();
      }
 	pass p2
      {		
 	vertexShader = compile vs_2_0 VS_L2(); 
 	pixelShader  = compile ps_2_0 PS_L2();
	AlphaBlendEnable = True;
 	SrcBlend = One;
 	DestBlend = One;
      }
 	pass p3
      {		
 	vertexShader = compile vs_2_0 VS_L3(); 
 	pixelShader  = compile ps_2_0 PS_L3();
	AlphaBlendEnable = True;
 	SrcBlend = One;
 	DestBlend = One;
      }
 	pass p4
      {		
 	vertexShader = compile vs_2_0 VS_L4(); 
 	pixelShader  = compile ps_2_0 PS_L4();
	AlphaBlendEnable = True;
 	SrcBlend = One;
 	DestBlend = One;
      }
 	pass p5
      {		
 	vertexShader = compile vs_2_0 VS_L5(); 
 	pixelShader  = compile ps_2_0 PS_L5();
	AlphaBlendEnable = True;
 	SrcBlend = One;
 	DestBlend = One;
      }
      }
   technique Light6
      {
 	pass p1
      {		
 	vertexShader = compile vs_2_0 VS_L1(); 
 	pixelShader  = compile ps_2_0 PS_L1();
      }
 	pass p2
      {		
 	vertexShader = compile vs_2_0 VS_L2(); 
 	pixelShader  = compile ps_2_0 PS_L2();
	AlphaBlendEnable = True;
 	SrcBlend = One;
 	DestBlend = One;
      }
 	pass p3
      {		
 	vertexShader = compile vs_2_0 VS_L3(); 
 	pixelShader  = compile ps_2_0 PS_L3();
	AlphaBlendEnable = True;
 	SrcBlend = One;
 	DestBlend = One;
      }
 	pass p4
      {		
 	vertexShader = compile vs_2_0 VS_L4(); 
 	pixelShader  = compile ps_2_0 PS_L4();
	AlphaBlendEnable = True;
 	SrcBlend = One;
 	DestBlend = One;
      }
 	pass p5
      {		
 	vertexShader = compile vs_2_0 VS_L5(); 
 	pixelShader  = compile ps_2_0 PS_L5();
	AlphaBlendEnable = True;
 	SrcBlend = One;
 	DestBlend = One;
      }
 	pass p6
      {		
 	vertexShader = compile vs_2_0 VS_L6(); 
 	pixelShader  = compile ps_2_0 PS_L6();
	AlphaBlendEnable = True;
 	SrcBlend = One;
 	DestBlend = One;
      }
      }
