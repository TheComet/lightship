// Simple vanishing concentric rings
// Created 30 October 2011.

float4x4 mWVP : WorldViewProjection;

float shrink = 1.0; // increase this to make the texture appear to shring towards the centre

texture baseTexture < string ResourceName = ""; >;

sampler baseSample = sampler_state 
{ texture = <baseTexture>;
  mipFilter = linear;
  magFilter = linear;
  minFilter = linear;
  addressU = clamp;
  addressV = clamp;
};

struct VSInput
{ float4 pos   : position;
  float2 UV    : texcoord0;
};

struct VSOutput
{ float4 pos   : position;
  float2 UV0   : texcoord0;
  float2 UV1   : texcoord1;
  float2 dist  : texcoord2;
};

struct PSOutput {  float4 col : Color; };

VSOutput VShader(VSInput In, VSOutput Out)
{ Out.pos = mul(In.pos, mWVP);
  Out.UV0 = In.UV;
  Out.dist = In.UV - 0.5.xx;
  Out.UV1 = 0.5.xx + Out.dist*shrink;
  return Out;
}

PSOutput PShader(VSOutput In, PSOutput Out)
{ float4 color = tex2D(baseSample, In.UV1);
  float fade = clamp(2.5*length(In.dist), 0, 1);
  fade = 5.0*fade*(1-fade)*color.a;
  Out.col = float4 (color.rgb*fade, fade);
  return Out;
}

technique test
{  pass p0
   {  vertexShader = compile vs_2_0 VShader();
      pixelShader  = compile ps_2_0 PShader();
      alphaBlendEnable = true;
      srcBlend = srcAlpha;
      destBlend = one;
      blendOp = add;
   }
}
